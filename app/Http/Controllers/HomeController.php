<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    private $data = [];

    public function __construct()
    {
        $this->data = [
            'title'             => 'Dashboard',
            'subtitle'          => '',
            'menu'              => 'Dashboard',
            'link_menu'         => '',
            'icon_menu'         => 'icon-home',
            'submenu'           => '',
            'link_submenu'      => '',
            'icon_submenu'      => '',
            'subsubmenu'        => '',
            'icon_subsubmenu'   => '',
            'route'             => 'dashboard',
            'permission'        => 'dashboard',
            'icon_primary'      => 'icon-home',
            'no'                => 1
        ];
        $this->statuses = ['waiting', 'approved', 'declined', 'on_progress', 'closed'];
    }

    public function index(Request $request)
    {
        $this->data['report'] = DB::table('report')->count();
        $this->data['users'] = DB::table('users')->count();
        $this->data['category'] = DB::table('category')->count();
        $this->data['risklevel'] = DB::table('risk_level')->count();
        $this->data['reportlevel'] = DB::table('report_level')->count();
        $this->data['total_nearmiss'] = DB::table('report')
            ->where(['category_id' => '1'])->count();
        $this->data['total_unsafe_condition'] = DB::table('report')
            ->where(['category_id' => '3'])->count();
        $this->data['total_unsafe_action'] = DB::table('report')
            ->where(['category_id' => '2'])->count();
        $this->data['total_waiting_approval'] = DB::table('report')
            ->where(['status' => 'waiting'])->count();
        $this->data['waiting_approval'] = DB::table('report')
            ->select('report.*', DB::raw("DATE_FORMAT(report.created_at, '%W, %d-%m-%Y. %H:%i') as date"))
            ->where(['status' => 'waiting'])->limit(10)->get();
        foreach ($this->statuses as $status) {
            $this->data['total_st_' . $status]
                = DB::table('report')
                ->where(['status' => $status])->count();
        }
        return view('home', $this->data);
    }
}
