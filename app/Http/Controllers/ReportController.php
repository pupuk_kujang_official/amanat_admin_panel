<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use DB;
use App\Report;
use App\ReportLevel;
use App\RiskLevel;
use App\Category;
use App\User;

class ReportController extends Controller
{
    private $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->data = [
            'title'             => 'Reporting',
            'subtitle'          => '',
            'menu'              => 'Reporting',
            'link_menu'         => '',
            'icon_menu'         => 'icon-list2',
            'submenu'           => '',
            'link_submenu'      => '',
            'icon_submenu'      => '',
            'subsubmenu'        => '',
            'icon_subsubmenu'   => '',
            'route'             => 'report',
            'permission'        => 'reporting',
            'icon_primary'      => '',
            'no'                => 1
        ];
        $this->statuses = ['waiting', 'approved', 'declined', 'on_progress', 'closed'];
        $this->middleware("permission:" . $this->data['permission'] . "-list", ['only' => ['index']]);
        $this->middleware("permission:" . $this->data['permission'] . "-create", ['only' => ['create', 'store']]);
        $this->middleware("permission:" . $this->data['permission'] . "-edit", ['only' => ['edit', 'update']]);
        $this->middleware("permission:" . $this->data['permission'] . "-delete", ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->data['datatable'] = Report::select('report.*', 'category.name as category_name', 'risk_level.name as risk_level_name')->join('risk_level', 'risk_level.id', '=', 'report.risk_level_id')->join('category', 'category.id', '=', 'report.category_id')->orderBy('id', 'DESC')->get();;
        return view($this->data['route'] . '.index', $this->data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['subtitle'] = 'Create Data';
        return view('components.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'poin' => 'required',
        ]);

        $query = Report::create([
            'name' => $request->input('name'),
            'poin' => $request->input('poin'),
            'is_need_evidence_after' => empty($request->input('is_need_evidence_after')) ? '0' : '1',
        ]);

        if ($query) {
            return response()->json([
                'status' => true,
                '_token' => csrf_token(),
                'message' => 'Add data success!',
                'return_url' => $request->input('submit') == 'submit' ? url($this->data['route'] . '/create') : url($this->data['route'])
            ]);
        } else {
            return response()->json([
                'status' => false,
                '_token' => csrf_token(),
                'message' => 'Add data fail!',
                'return_url' => '#'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $single = Report::find($id);
        $this->data['subtitle'] = 'Edit Data';
        $this->data['data_row'] = $single;
        $this->data['category'] = Category::find($single->category_id);
        $this->data['report_level'] = ReportLevel::find($single->report_level_id);
        $this->data['risk_level'] = RiskLevel::find($single->risk_level_id);
        $this->data['status_list'] = $this->statuses;
        $this->data['category_list'] = Category::get();
        $this->data['report_level_list'] = ReportLevel::get();
        $this->data['risk_level_list'] = RiskLevel::get();
        $usr = new User();
        $this->data['emps'] = $usr->getEmp();
        return view('components.edit', $this->data);
    }

    public function print($id = null)
    {
        $single = Report::find($id);
        $this->data['data_row'] = $single;
        $this->data['category'] = Category::find($single->category_id);
        $this->data['report_level'] = ReportLevel::find($single->report_level_id);
        $this->data['risk_level'] = RiskLevel::find($single->risk_level_id);
        return view($this->data['route'] . '.print', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $this->validate($request, [
            'status' => 'required',
            'category_id' => 'required',
            'report_level_id' => 'required',
            'risk_level_id' => 'required',
            'executed_by' => 'nullable',
        ]);

        $query = Report::find($id);

        if ($request->input('status') == 'declined') {
            $this->validate($request, [
                'decline_note' => 'required',
            ]);
            $query->decline_note = $request->input('decline_note');
        }

        $status_before = $query->status;

        $query->category_id = $request->input('category_id');
        $query->report_level_id = $request->input('report_level_id');
        $query->risk_level_id = $request->input('risk_level_id');

        $report_level = ReportLevel::find($query->report_level_id);
        if ($report_level) {
            $query->report_level_poin = $report_level->poin;
        }

        $risk_level = RiskLevel::find($query->risk_level_id);
        if ($risk_level) {
            $query->risk_level_poin = $risk_level->poin;
        }

        if ($request->input('status') != $status_before) {
            if ($query->is_phl_request == 0) {
                $this->_push_notif_demplon([
                    'no_badge' => $query->issuer_badge,
                    'title' => 'Update Status Report Amanat',
                    'body' => ':nama:, report amanat anda dengan kode ' . $query->code . ' telah berubah status dari ' . $status_before . ' menjadi ' . $request->input('status') . ''
                ]);
            }
        }

        if ($request->input('status') == 'closed') {
            $query->poin = (int) $query->report_level_poin + (int) $query->risk_level_poin;
            if ($status_before == 'approved' || $status_before == 'on_progress') {
                if ($query->is_phl_request == 0) {
                    //send kerah poin
                    $this->auto_poin_kerah([
                        'no_badge' => $query->issuer_badge,
                        'param' => $query->poin,
                    ]);
                }
            }
        }

        $kirim_notif_ke_executor = false;
        if ($query->is_phl_request == 0) {
            if ($query->executed_by == '') {
                $kirim_notif_ke_executor = true;
            } else {
                if ($request->input('executed_by') != '') {
                    if ($request->input('executed_by') != $query->executed_by) {
                        $kirim_notif_ke_executor = true;
                    }
                }
            }
        }

        if ($kirim_notif_ke_executor && $request->input('executed_by') != '') {
            $this->_push_notif_demplon([
                'no_badge' => $request->input('executed_by'),
                'title' => 'New Assignment Report Amanat',
                'body' => ':nama:, report amanat dengan kode ' . $query->code . ' telah diassign kepada anda untuk ditindaklanjuti'
            ]);
        }

        $query->executed_by = $request->input('executed_by');
        $query->status = $request->input('status');

        if ($request->hasFile('evidence_after')) {
            $original_filename = $request->file('evidence_after')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);
            $destination_path = './uploads/evidence/after/';
            $imageName = date('Y-m-d_H-i-s') . '_' . Str::random(10) . '.' . $file_ext;

            $request->file('evidence_after')->move($destination_path, $imageName);
            $this->upload_evidence_after($destination_path . $imageName, $query->id);
        }

        $query->save();

        if ($query) {
            return response()->json([
                'status' => true,
                '_token' => csrf_token(),
                'message' => 'Update data success!',
                'return_url' => url($this->data['route'] . '/' . $id . '/edit')
            ]);
        } else {
            return response()->json([
                'status' => false,
                '_token' => csrf_token(),
                'message' => 'Update data fail!',
                'return_url' => '#'
            ]);
        }
    }

    private function _push_notif_demplon($param = null)
    {
        if ($param == null) {
            return true;
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apis.pupuk-kujang.co.id/demplon/hrdmobile/panon/sendsinglenotif/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'type' => '1',
                'app' => '16',
                'karyawan' => $param['no_badge'],
                'title' => $param['title'],
                'body' => $param['body'],
                'atasan_title' => '',
                'atasan_body' => '',
                'link' => '',
                'attachment' => ''
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    private function upload_evidence_after($file_path = null, $report_id = null)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/upload/after/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 500,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'report_id' => $report_id,
                'evidence_after' => curl_file_create($file_path)
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    private function auto_poin_kerah($param)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apis.pupuk-kujang.co.id/kerah/public/api/activity/insert_auto',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'no_badge=' . $param['no_badge'] . '&param=' . $param['param'],
            CURLOPT_HTTPHEADER => array(
                'Authorization: cb9995df07f73b2edfd86f5e60026457'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null)
    {
        $query = Report::where('id', $id)->delete();
        if ($query) {
            return response()->json([
                'status' => true,
                '_token' => csrf_token(),
                'message' => 'Delete data success!',
                'return_url' => url($this->data['route'])
            ]);
        } else {
            return response()->json([
                'status' => false,
                '_token' => csrf_token(),
                'message' => 'Delete data fail!',
                'return_url' => '#'
            ]);
        }
    }

    /**
     * Display a one of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_by_kecamatan(Request $request, $id = null)
    {
        $data  = Report::where('kecamatan_id', $id)->get();
        echo "<option value=''>Choose One</option>";
        foreach ($data as $key => $value) {
            echo "<option value='$value->id'>$value->name</option>";
        }
    }
}
