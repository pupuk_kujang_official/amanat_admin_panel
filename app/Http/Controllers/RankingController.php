<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Report;
use App\ReportLevel;
use App\RiskLevel;
use App\Category;

class RankingController extends Controller
{
    private $data = [];

    public function __construct()
    {
        $this->data = [
            'title'             => 'Ranking',
            'subtitle'          => '',
            'menu'              => 'Ranking',
            'link_menu'         => '',
            'icon_menu'         => 'icon-trophy2',
            'submenu'           => '',
            'link_submenu'      => '',
            'icon_submenu'      => '',
            'subsubmenu'        => '',
            'icon_subsubmenu'   => '',
            'route'             => 'ranking',
            'permission'        => 'ranking',
            'icon_primary'      => '',
            'no'                => 1
        ];
        $this->middleware("permission:" . $this->data['permission'] . "-list", ['only' => ['index']]);
        $this->middleware("permission:" . $this->data['permission'] . "-create", ['only' => ['create', 'store']]);
    }

    public function index(Request $request)
    {
        $this->data['subtitle'] = ucfirst('Silahkan pilih periode di atas');

        $start = $request->input('start');
        $end = $request->input('end');
        $this->data['start'] = $start == "" ? date('Y-m-d') : $start;
        $this->data['end'] = $end == "" ? date('Y-m-d') : $end;
        $this->data['datatable_emp'] = [];
        $this->data['datatable_org'] = [];

        if ($start == "" || $end == "") {
            return view($this->data['route'] . '.index', $this->data);
        }

        $this->data['datatable_emp'] = DB::select(DB::raw("SELECT
                                                    *
                                                    FROM (
                                                        SELECT
                                                        p.issuer_badge,
                                                        p.issuer_name,
                                                        sum(p.poin) as poin,
                                                        p.updated_at
                                                        FROM report p
                                                        WHERE status = 'closed'
                                                        AND DATE(updated_at) >= '$start'
                                                        AND DATE(updated_at) <= '$end'
                                                        GROUP BY issuer_badge
                                                        ORDER BY updated_at ASC
                                                    ) d order by poin DESC, updated_at ASC"));
        $this->data['datatable_org'] = DB::select(DB::raw("SELECT
                                                    *
                                                    FROM (
                                                        SELECT
                                                        p.issuer_unit_code,
                                                        p.issuer_unit_name,
                                                        sum(p.poin) as poin,
                                                        p.updated_at
                                                        FROM report p
                                                        WHERE status = 'closed'
                                                        AND DATE(updated_at) >= '$start'
                                                        AND DATE(updated_at) <= '$end'
                                                        GROUP BY issuer_unit_code
                                                        ORDER BY updated_at ASC
                                                    ) d order by poin DESC, updated_at ASC"));

        $start = date('d F Y', strtotime($start));
        $end = date('d F Y', strtotime($end));
        $this->data['subtitle'] = ucfirst("Menampilkan ranking pada periode <b> $start s.d. $end </b>");
        return view($this->data['route'] . '.index', $this->data);
    }

    public function store(Request $request)
    {
        $this->data['subtitle'] = ucfirst('Silahkan pilih periode di atas');

        $start = $request->input('start');
        $end = $request->input('end');
        $this->data['start'] = $start == "" ? date('Y-m-d') : $start;
        $this->data['end'] = $end == "" ? date('Y-m-d') : $end;
        $this->data['datatable_emp'] = [];
        $this->data['datatable_org'] = [];

        if ($start == "" || $end == "") {
            return view($this->data['route'] . '.index', $this->data);
        }

        $this->data['datatable_emp'] = DB::select(DB::raw("SELECT
                                                    *
                                                    FROM (
                                                        SELECT
                                                        p.issuer_badge,
                                                        p.issuer_name,
                                                        sum(p.poin) as poin,
                                                        p.updated_at
                                                        FROM report p
                                                        WHERE status = 'closed'
                                                        AND DATE(updated_at) >= '$start'
                                                        AND DATE(updated_at) <= '$end'
                                                        GROUP BY issuer_badge
                                                        ORDER BY updated_at ASC
                                                    ) d order by poin DESC, updated_at ASC"));
        $this->data['datatable_org'] = DB::select(DB::raw("SELECT
                                                    *
                                                    FROM (
                                                        SELECT
                                                        p.issuer_unit_code,
                                                        p.issuer_unit_name,
                                                        sum(p.poin) as poin,
                                                        p.updated_at
                                                        FROM report p
                                                        WHERE status = 'closed'
                                                        AND DATE(updated_at) >= '$start'
                                                        AND DATE(updated_at) <= '$end'
                                                        GROUP BY issuer_unit_code
                                                        ORDER BY updated_at ASC
                                                    ) d order by poin DESC, updated_at ASC"));

        $start = date('d F Y', strtotime($start));
        $end = date('d F Y', strtotime($end));
        $this->data['subtitle'] = ucfirst("Menampilkan ranking pada periode <b> $start s.d. $end </b>");
        return view($this->data['route'] . '.index', $this->data);
    }
}
