<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Http;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasRoles;

    public $api_emp = 'https://portal.pupuk-kujang.co.id/apps/api_hr/public/';

    public $token_api_emp = 'f252f169933b9a719a0bbe13de9d249f';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'phone_number', 'avatar', 'is_suspend', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getEmp($param = null)
    {
        $response  = Http::withHeaders([
            'Authorization' => $this->token_api_emp,
        ])->get($this->api_emp . 'api/master/emp/' . $param);
        return json_decode($response);
    }

    public function getOrg($param = null)
    {
        $response  = Http::withHeaders([
            'Authorization' => $this->token_api_emp,
        ])->get($this->api_emp . 'api/master/org/' . $param);
        return json_decode($response);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = is_object(Auth::guard(config('web'))->user()) ? Auth::guard(config('web'))->user()->id : 1;
            $model->updated_by = NULL;
        });

        static::updating(function ($model) {
            $model->updated_by = is_object(Auth::guard(config('web'))->user()) ? Auth::guard(config('web'))->user()->id : 1;
        });
    }
}
