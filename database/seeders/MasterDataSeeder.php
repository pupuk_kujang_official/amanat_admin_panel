<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\ReportLevel;
use App\RiskLevel;
use App\Category;
use App\Report;


class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Nearmiss']);
        Category::create(['name' => 'Unsafe Action']);
        Category::create(['name' => 'Unsafe Condition']);

        ReportLevel::create(['name' => 'Langsung ditindaklanjuti', 'poin' => 2, 'is_need_evidence_after' => '1']);
        ReportLevel::create(['name' => 'Diteruskan', 'poin' => 1, 'is_need_evidence_after' => '0']); 

        RiskLevel::create(['name' => 'Rendah', 'poin' => 1]);
        RiskLevel::create(['name' => 'Sedang', 'poin' => 2]);
        RiskLevel::create(['name' => 'Tinggi', 'poin' => 3]);
        

    }
}

