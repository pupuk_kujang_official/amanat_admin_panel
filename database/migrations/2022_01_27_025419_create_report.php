<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10)->unique();
            $table->enum('status', ['waiting', 'approved', 'declined', 'on_progress', 'closed'])->default('waiting');
            $table->enum('progress', ['received', 'on_process', 'closed'])->default('received');
            $table->enum('is_phl_issuer', [1, 0])->default(0);
            $table->date('date');
            $table->time('time');
            $table->string('location', 255)->nullable();
            $table->text('desc')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('report_level_id')->nullable();
            $table->tinyInteger('report_level_poin')->default(0);
            $table->unsignedBigInteger('risk_level_id');
            $table->tinyInteger('risk_level_poin')->default(0);
            $table->string('issuer_badge', 255);
            $table->string('issuer_name', 255);
            $table->string('issuer_unit_code', 255)->nullable();
            $table->string('issuer_unit_name', 255)->nullable();
            $table->string('evidence_before', 255);
            $table->string('evidence_after', 255)->nullable();

            $table->tinyInteger('poin')->default(0);
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report');
    }
}
