@extends('layouts.app')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-blue-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{ $users }}</h3>
                        <span class="text-uppercase font-size-xs">users</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-users icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-danger-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{ $category }} </h3>
                        <span class="text-uppercase font-size-xs">total category</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-grid5 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-green-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{ $reportlevel }} </h3>
                        <span class="text-uppercase font-size-xs">total report level</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-price-tags2 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-purple-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{ $risklevel }} </h3>
                        <span class="text-uppercase font-size-xs">total risk level</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-popout icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="row">
                <div class="col-md-4">
                    <div class="card bg-teal text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <h3 class="font-weight-semibold mb-0">{{ $total_nearmiss }}</h3>
                            </div>
                            <div>
                                Nearmiss
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-grey text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <h3 class="font-weight-semibold mb-0">{{ $total_unsafe_action }}</h3>
                            </div>
                            <div>
                                Unsafe Action
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-purple text-white">
                        <div class="card-body">
                            <div class="d-flex">
                                <h3 class="font-weight-semibold mb-0">{{ $total_unsafe_condition }}</h3>
                            </div>
                            <div>
                                Unsafe Condition
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h6 class="card-title">Summary by Status</h6>
                        </div>

                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="w-100">Status</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="mr-3">
                                                    <a href="#" class="btn btn-primary rounded-pill btn-icon btn-sm">
                                                        <span class="letter-icon">#</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-body font-weight-semibold letter-icon-title">All</a>
                                                    <div class="text-muted font-size-sm"><i class="icon-list2 font-size-sm mr-1"></i> All Report</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-semibold mb-0 text-right">{{ $report }}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="mr-3">
                                                    <a href="#" class="btn bg-orange rounded-pill btn-icon btn-sm">
                                                        <span class="letter-icon">W</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-body font-weight-semibold letter-icon-title">Waiting</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-semibold mb-0 text-right">{{ $total_st_waiting }}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="mr-3">
                                                    <a href="#" class="btn bg-info rounded-pill btn-icon btn-sm">
                                                        <span class="letter-icon">A</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-body font-weight-semibold letter-icon-title">Approved</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-semibold mb-0 text-right">{{ $total_st_approved }}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="mr-3">
                                                    <a href="#" class="btn bg-dark rounded-pill btn-icon btn-sm">
                                                        <span class="letter-icon">O</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-body font-weight-semibold letter-icon-title">On Progress</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-semibold mb-0 text-right">{{ $total_st_on_progress }}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="mr-3">
                                                    <a href="#" class="btn bg-danger rounded-pill btn-icon btn-sm">
                                                        <span class="letter-icon">D</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-body font-weight-semibold letter-icon-title">Declined</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-semibold mb-0 text-right">{{ $total_st_declined }}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="mr-3">
                                                    <a href="#" class="btn bg-success rounded-pill btn-icon btn-sm">
                                                        <span class="letter-icon">C</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-body font-weight-semibold letter-icon-title">Closed</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-semibold mb-0 text-right">{{ $total_st_closed }}</h6>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="card">
                <!-- Card heading -->
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Menunggu tindak lanjut <br /> {!! $total_waiting_approval > 0 ? '<small class="text-muted">Ada '.$total_waiting_approval.' report yang menunggu tindak lanjut</small>' : '<small class="text-muted">Tidak ada antrian tindak lanjut</small>' !!} </h5>

                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <!-- /card heading -->
                <!-- content -->
                <div class="card-body">
                    <ul class="media-list">
                        @if(count($waiting_approval) == 0)
                        <div class="alert alert-info border-0 alert-dismissible">
                            <span class="font-weight-semibold">Info</span><br /> Belum ada antrian tindak lanjut saat ini.
                        </div>
                        @else
                        @foreach($waiting_approval as $wv)
                        <li class="media">
                            <div class="mr-3 mt-2">
                                <img src="https://portal.pupuk-kujang.co.id/apps/picemp/{{ $wv->issuer_badge }}.jpg" class="rounded-circle backup_picture" width="40" height="40" alt="">
                            </div>

                            <div class="media-body">
                                <div class="d-flex justify-content-between">
                                    <a href="#" class="text-dark">{{ $wv->issuer_badge }} - {{ $wv->issuer_name }}</a>
                                </div>
                                {{ $wv->desc }} - {{ $wv->location }}<br />
                                <span class="font-size-sm text-muted">{{ $wv->date }} WIB</span>
                            </div>

                            <div class="align-self-center ml-3">
                                <div class="list-icons list-icons-extended">
                                    <a href="{{ url('/report/'.$wv->id.'/edit') }}" class="list-icons-item" data-popup="tooltip" title="Lihat Detail"><i class="icon-forward"></i></a>
                                </div>
                            </div>
                        </li>
                        @endforeach
                        @if($total_waiting_approval > 10)
                        <a href="{{ url('report') }}" class="mt-3 btn bg-teal-400 btn-block">See All Data</a>
                        @endif
                        @endif
                    </ul>
                </div>
                <!-- / content -->
            </div>
        </div>
    </div>
</div>
@endsection