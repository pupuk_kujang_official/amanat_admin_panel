<div class="row mb-0">
    <div class="col-sm-4">
        <h5>Status Terakhir <br />
            <div class="btn btn-sm bg-dark">{{$data_row->status}}</div>
        </h5>
    </div>
    <div class="col-sm-4">
        <h5>Poin<br />
            <div class="btn btn-sm bg-success">{{$data_row->poin}}</div>
        </h5>
    </div>
    <div class="col-sm-4">
        <div class="text-sm-right" style="display:inline">
            <div id="created_at">Dibuat pada {{$data_row->created_at}}</div>
            <div id="updated_at">Perubahan terakhir {{$data_row->updated_at}}</div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route($route.'.print', $data_row->id) }}" target="_blank" class="btn btn-success btn-lg">Print Document</a>
                <div class="media" style="margin-top:10px">
                    <div class="mr-3" id="requestor_avatar_url">
                        <img src="https://portal.pupuk-kujang.co.id/apps/picemp/{{$data_row->issuer_badge}}.jpg" class="rounded-circle backup_picture " style="width:60px;height:60px;">
                    </div>
                    <div class="media-body mt-1">
                        <div class="d-flex justify-content-between">
                            <span id="requestor">Issuer : <b>{{$data_row->issuer_name}} ({{$data_row->issuer_badge}})</b></span>
                        </div>
                        <span id="requestor_org" class="mt-2" style="width:80%;">{{$data_row->issuer_unit_name}} ({{$data_row->issuer_unit_code}})</span>
                    </div>
                </div>
                <div class="mt-2" style="width:100%"><i class="icon-checkmark mr-1"></i>Category <b>{{$category->name}}</b> </div>
                <div class="mt-2" style="width:100%"><i class="icon-bookmark mr-1"></i>Report Level <b>{{$report_level->name}}</b> </div>
                @if($risk_level)
                <div class="mt-2" style="width:100%"><i class="icon-attachment mr-1"></i>Risk Level <b>{{$risk_level->name}}</b> </div>
                @endif
                <div class="mt-2" style="width:100%"><i class="icon-location3 mr-1"></i>Lokasi <b>{{$data_row->location}}</b> </div>
                <div class="mt-2" style="width:100%"><i class="icon-pencil mr-1"></i>Deskripsi <b>{{$data_row->desc}}</b> </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-sm-right">
                            <h6 class="text-dark "><b>Evidence Before:</b> </h6>
                            <div class="card" style="background-color:#808080">
                                <div class="card-img-actions m-1">
                                    @if($data_row->evidence_before)
                                    <img class="card-img img-fluid" src="{{ 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/before/'.$data_row->evidence_before }}" style="min-height:200px;color:#fff" alt="Attachment not found">
                                    <div class="card-img-actions-overlay card-img">
                                        <a href="_blank" href="{{ 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/before/'.$data_row->evidence_before }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                            <i class="icon-zoomin3"></i>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            </li>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-sm-right">
                            <h6 class="text-dark "><b>Evidence After:</b> </h6>
                            <div class="card" style="background-color:#808080">
                                <div class="card-img-actions m-1">
                                    @if($data_row->evidence_after)
                                    <img class="card-img img-fluid" src="{{ 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/after/'.$data_row->evidence_after }}" style="min-height:200px;color:#fff" alt="Attachment not found">
                                    <div class="card-img-actions-overlay card-img">
                                        <a href="_blank" href="{{ 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/after/'.$data_row->evidence_after }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                            <i class="icon-zoomin3"></i>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            </li>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@if($data_row->status!='closed')
<form style="margin-top:30px" action="{{ $subtitle == 'Edit Data' ? route($route.'.update_process', @$data_row->id) : route($route.'.store') }}" method="post" class="form-validate-ajax" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="font-weight-semibold col-md-2 col-form-label">Status <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <select name="status" id="status" class="form-control  select-search" required>
                        <option value="">Choose One</option>
                        @foreach($status_list as $d)
                        <option value="{{ $d }}" {{ $d == @$data_row->status ? 'selected' : '' }}> {{ $d }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row" id="decline-note-container" style="display:none">
                <label class="font-weight-semibold col-md-2 col-form-label">Decline Note</label>
                <div class="col-md-10">
                    <textarea type="text" class="form-control" id="decline_note" name="decline_note" placeholder="Enter decline note" value="{{ @$data_row->decline_note }}"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="font-weight-semibold col-md-2 col-form-label">Category <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <select name="category_id" id="category_id" class="form-control  select-search" required>
                        <option value="">Choose One</option>
                        @foreach($category_list as $d)
                        <option value="{{ $d->id }}" {{ $d->id == @$data_row->category_id ? 'selected' : '' }}> {{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="font-weight-semibold col-md-2 col-form-label">Report Level <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <select name="report_level_id" id="report_level_id" class="form-control  select-search" required>
                        <option value="">Choose One</option>
                        @foreach($report_level_list as $d)
                        <option value="{{ $d->id }}" {{ $d->id == @$data_row->report_level_id ? 'selected' : '' }}> {{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="font-weight-semibold col-md-2 col-form-label">Risk Level <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <select name="risk_level_id" id="risk_level_id" class="form-control  select-search" required>
                        <option value="">Choose One</option>
                        @foreach($risk_level_list as $d)
                        <option value="{{ $d->id }}" {{ $d->id == @$data_row->risk_level_id ? 'selected' : '' }}> {{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @if($data_row->report_level_id=='2')
            <div class="form-group row">
                <label class="font-weight-semibold col-md-2 col-form-label">Change Evidence After</label>
                <div class="col-md-10">
                    <input type="file" name="evidence_after" id="evidence_after" class="file-input-extensions" data-show-caption="false" data-show-upload="false" data-fouc>
                    <span class="form-text text-muted"> Only file <code>jpg</code> and <code>jpeg</code> extensions are allowed</span>
                </div>
            </div>
            @endif
            <div class="form-group row">
                <label class="font-weight-semibold col-md-2 col-form-label">Executed By</label>
                <div class="col-md-10">
                    <select name="executed_by" id="executed_by" class="form-control  select-search">
                        <option value="">Choose One</option>
                        @foreach($emps as $d)
                        <option value="{{ $d->NO_BADGE }}" {{ $d->NO_BADGE == @$data_row->executed_by ? 'selected' : '' }}> {{ $d->NAMA }} ({{ $d->NO_BADGE }}) ({{ $d->UNIT_INDUK }})</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <br />
            <div class="form-group">
                <input type="hidden" name="submit" id="submit-type" value="submit" />
                <button type="submit" id="submit" value="submit" class="btn bg-transparent text-blue border-blue mr-2 btn-submit" onclick="(function(){$('#submit-type').val('submit');return true;})();return true;">Submit<i class="icon-paperplane ml-2"></i></button>
                <button type="submit" id="submit-back" value="submit-back" class="btn bg-transparent text-blue border-blue mr-2 btn-submit-back" onclick="(function(){$('#submit-type').val('submit-back');return true;})();return true;">Submit & Back<i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(() => {
        $('#status').change(function() {
            if ($(this).val() == "declined") {
                $("#decline-note-container").show()
            } else {
                $("#decline-note-container").hide()
            }
        })
    })
</script>

@endif