<html>

<head>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/material/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <script type="text/javascript">
        window.print();
        window.onfocus = function() {
            window.close();
        }
    </script>
</head>

<body style="padding:20px">
    <table class="table table-bordered">
        <tbody>
            <tr>
                <th scope="row">Document Name</th>
                <td>Amanat Report</td>
            </tr>
            <tr>
                <th scope="row">ID</th>
                <td>{{ $data_row->id }}</td>
            </tr>
            <tr>
                <th scope="row">Code</th>
                <td>{{ $data_row->code }}</td>
            </tr>
            <tr>
                <th scope="row">Created</th>
                <td>{{ $data_row->created_at }}</td>
            </tr>
            <tr>
                <th scope="row">Updated</th>
                <td>{{ $data_row->updated_at }}</td>
            </tr>
            <tr>
                <th scope="row">Issuer</th>
                <td>
                    <div class="media" style="margin-top:10px">
                        <div class="mr-3" id="requestor_avatar_url">
                            <img src="https://portal.pupuk-kujang.co.id/apps/picemp/{{$data_row->issuer_badge}}.jpg" class="rounded-circle backup_picture " style="width:60px;height:60px;">
                        </div>
                        <div class="media-body mt-1">
                            <div class="d-flex justify-content-between">
                                <span id="requestor">Issuer : <b>{{$data_row->issuer_name}} ({{$data_row->issuer_badge}})</b></span>
                            </div>
                            <span id="requestor_org" class="mt-2" style="width:80%;">{{$data_row->issuer_unit_name}} ({{$data_row->issuer_unit_code}})</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">Status</th>
                <td>{{ $data_row->status }}</td>
            </tr>
            <tr>
                <th scope="row">Poin</th>
                <td>{{ $data_row->poin }}</td>
            </tr>
            <tr>
                <th scope="row">Executed By</th>
                <td>{{ $data_row->executed_by }}</td>
            </tr>
            <tr>
                <th scope="row">Category</th>
                <td>{{ $category->name }}</td>
            </tr>
            <tr>
                <th scope="row">Report Level</th>
                <td>{{ $report_level->name }}</td>
            </tr>
            <tr>
                <th scope="row">Risk Level</th>
                <td>{{ $risk_level->name }}</td>
            </tr>
            <tr>
                <th scope="row">Location</th>
                <td>{{ $data_row->location }}</td>
            </tr>
            <tr>
                <th scope="row">Description</th>
                <td>{{ $data_row->desc }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table table-bordered">
        <tr>
            <td style="width:50%">
                <div class="text-sm-center">
                    <h6 class="text-dark "><b>Evidence Before:</b> </h6>
                    @if($data_row->evidence_before)
                    <img class="card-img" src="{{ 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/before/'.$data_row->evidence_before }}" style="width:300px;color:#fff" alt="Attachment not found">
                    @endif
                </div>
            </td>
            <td style="width:50%">
                <div class="text-sm-center">
                    <h6 class="text-dark "><b>Evidence After:</b> </h6>
                    @if($data_row->evidence_after)
                    <img class="card-img" src="{{ 'https://apis.pupuk-kujang.co.id/amanat/public/api/evidence/after/'.$data_row->evidence_after }}" style="width:300px;color:#fff" alt="Attachment not found">
                    @endif
                </div>
            </td>
        </tr>
    </table>
</body>

</html>