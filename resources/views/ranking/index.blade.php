@extends('layouts.app')

@section('content')

<!-- Content area -->
<div class="content">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Silahkan pilih periode</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route($route.'.index') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label class="font-weight-semibold">Dari tanggal <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" id="start" name="start" placeholder="" value="{{ @$start }}" required>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label class="font-weight-semibold">Sampai tanggal <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" id="end" name="end" placeholder="" value="{{ @$end }}" required>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn bg-teal btn-block btn-search mt-4"><i class="icon-search4 mr-2"></i> Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><b>{{ $title }}</b> <br /><small> {!! $subtitle !!} </small></h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-primary border-0 alert-dismissible">
                    <span class="font-weight-semibold">Info!</span> <br />Karyawan atau Unit kerja yang masuk ranking adalah mereka yang sudah memiliki minimal 1 Poin.
                </div>
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="nav-item"><a href="#highlighted-tab1" class="nav-link active" data-toggle="tab">Ranking Per Karyawan</a></li>
                    <li class="nav-item"><a href="#highlighted-tab2" class="nav-link " data-toggle="tab">Ranking Per Unit Kerja</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="highlighted-tab1" style="margin-top:-20px;">
                        <table class="table datatable-button-html5-columns no-wrap">
                            <thead>
                                <tr>
                                    <th width="15%">Ranking</th>
                                    <th width="70%">Employee</th>
                                    <th width="25%">Poin</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($datatable_emp as $row)
                                <tr>
                                    <td class="">{{ $no }} </td>
                                    <td>
                                        <img src="https://portal.pupuk-kujang.co.id/apps/picemp/{{ $row->issuer_badge }}.jpg" class="rounded-circle mt-2 backup_picture" width="40" height="40" alt="">
                                        {{ $row->issuer_name}} ({{ $row->issuer_badge }} )
                                    </td>
                                    <td><span class="badge badge-primary badge-pill">{{ $row->poin }}</span></td>
                                </tr>
                                <?php $no++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane fade" id="highlighted-tab2">
                        <table class="table datatable-button-html5-columns no-wrap">
                            <thead>
                                <tr>
                                    <th width="15%">Ranking</th>
                                    <th width="70%">Unit</th>
                                    <th width="25%">Poin</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($datatable_org as $row)
                                <tr>
                                    <td class="">{{ $no}}</td>
                                    <td class="">{{ $row->issuer_unit_name }} ({{ $row->issuer_unit_code }})</td>
                                    <td><span class="badge badge-primary badge-pill">{{ $row->poin }}</span></td>
                                </tr>
                                <?php $no++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(() => {
        $('form').submit(() => {
            $('button[type=submit]').prop('disabled', true).html('<i class="fa fa-spinner fa-spin"></i>  Loading...');
        })
    })
</script>
@endsection