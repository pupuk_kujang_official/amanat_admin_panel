
## Built with

- Laravel 8
- Laravel permission by spatie
- Limitless Bootstrap 4

## How to Install

- Run `composer install`
- Run `npm install` (optional)
- Run `php artisan key:generate`
- setup .env from .env-example
- Run `php artisan migrate:fresh --seed`
- Run `php artisan serve` to start app
- Default account username: `superadmin`, password: `123123123`

## Flow Diagram & Business Process 

- Draw IO : https://drive.google.com/file/d/1xPMzoJH-JYxN-ASe1p2idL3MqIDrX35a/view
- Dibuat oleh Pak Abdurrahman (K3)